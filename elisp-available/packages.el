(package-initialize)

(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(defvar my-packages
  '(company
    emmet-mode
    flycheck
    js2-mode
    json-mode
    magit
    markdown-mode
    ;; org-mime
    ox-json
    prettier-js
    tide
    yaml-mode
    web-mode
    yasnippet
    zenburn-theme))

(setq package-selected-packages my-packages)

;; Install packages from “my-packages” that are missing. Run
;; package-refresh-contents automatically on (only) the first install.
(defun my-package-refresh-contents (&rest _) (package-refresh-contents) (advice-remove 'package-install #'my-package-refresh-contents))
(advice-add 'package-install :before #'my-package-refresh-contents)
(cl-loop for my-package in my-packages unless (package-installed-p my-package) do (package-install my-package))

(load-theme 'zenburn t)
(set-face-attribute 'mode-line nil :background "#2b2b2b" :foreground "#8fb28f" :box '(:line-width -1 :color "#2b2b2b") :height 140)
(set-face-attribute 'mode-line-inactive nil :background "#383838" :foreground "#5f7f5f" :box '(:line-width -1 :color "#383838"))
(set-face-attribute 'fill-column-indicator nil :foreground "#2b2b2b")

(global-set-key (kbd "C-x g") 'magit-status)

(add-hook 'web-mode-hook 'emmet-mode)
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

(setq-default js2-mode-show-parse-errors nil)
(setq-default js2-mode-show-strict-warnings nil)

(setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc))

(global-flycheck-mode 1)

(set-face-attribute 'flycheck-error nil :underline '(:color "#ca6a6a"))
(set-face-attribute 'flycheck-warning nil :underline '(:color "#cec369"))

(setq-default flycheck-highlighting-mode 'lines)

(flycheck-add-mode 'javascript-eslint 'web-mode)
(flycheck-add-mode 'javascript-eslint 'js2-mode)

(add-hook 'js2-mode-hook 'prettier-js-mode)
(add-hook 'web-mode-hook 'prettier-js-mode)

(setq-default web-mode-markup-indent-offset 2)

(setq-default prettier-js-args '(
  "--no-semi"
  "--single-quote"
  "--trailing-comma"
  "none"
  "--vue-indent-script-and-style"
))

;; Begin local ESLint workaround (Source: https://github.com/flycheck/flycheck/issues/1428#issuecomment-591320954)
(defun flycheck-node_modules-executable-find (executable)
  (or
   (let* ((base (locate-dominating-file buffer-file-name "node_modules"))
          (cmd (if base (expand-file-name (concat "node_modules/.bin/" executable) base))))
     (if (and cmd (file-exists-p cmd))
         cmd))
   (flycheck-default-executable-find executable)))

(define-key flycheck-mode-map (kbd "C-c f") 'flycheck-next-error)
(define-key flycheck-mode-map (kbd "C-c F") #'flycheck-previous-error)
(define-key flycheck-mode-map (kbd "C-c M-f") #'flycheck-list-errors)

(require 'tide) ;; Fixes flycheck-add-next-checker crash
(flycheck-add-next-checker 'javascript-eslint 'javascript-tide 'append)

(add-hook 'markdown-mode-hook 'visual-line-mode)

(setq-default markdown-command "/usr/bin/pandoc")

(add-hook 'js2-mode-hook (lambda () (setq-default js-indent-level 2)))

(add-to-list 'auto-mode-alist '("\\.vue?\\'" . web-mode))

(add-hook 'web-mode-hook (lambda ()
                           (setq-default web-mode-code-indent-offset 2)
                           (setq-default web-mode-css-indent-offset 2)
                           (setq-default web-mode-markup-indent-offset 2)
                           (setq-default web-mode-script-padding 2)))

(setq-default yas-snippet-dirs '("~/.emacs.d/snippets"))
(yas-global-mode 1)

(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.scss?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue?\\'" . web-mode))

(add-hook 'web-mode-hook #'(lambda ()
                             (setq-default require-final-newline 'visit-save)
                             (setq-default mode-require-final-newline 'visit-save)))

(defun my-node_modules-flycheck-hook ()
  (setq-local flycheck-executable-find #'flycheck-node_modules-executable-find))

(add-hook 'js2-mode-hook 'my-node_modules-flycheck-hook)
(add-hook 'js-mode-hook 'my-node_modules-flycheck-hook)
(add-hook 'web-mode-hook 'my-node_modules-flycheck-hook)
;; End local ESLint workaround

;; Begin Tide configuration (Source: https://github.com/ananthakumaran/tide)
(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq-default flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  (company-mode +1))

(add-hook 'js2-mode-hook #'setup-tide-mode)
;; End Tide configuration

(require 'ox-json)
