(require 'mu4e)

(setq mail-default-headers "Cc: \nBcc: \n"
      mu4e-change-filenames-when-moving t
      mu4e-compose-format-flowed t
      mu4e-drafts-folder "/Drafts"
      mu4e-get-mail-command "mbsync jon@nejla.com"
      mu4e-headers-include-related nil
      mu4e-maildir ".mail/jon@nejla.com"
      mu4e-maildir-shortcuts '(("/Archive" . ?a)
                               ("/Drafts" . ?d)
                               ("/INBOX" . ?i)
                               ("/Sent" . ?s)
                               ("/Trash" . ?t))
      mu4e-refile-folder "/Archive"
      mu4e-sent-folder "/Sent"
      mu4e-trash-folder "/Trash"
      mu4e-update-interval (* 5 60)
      user-mail-address "jon@nejla.com"
      user-full-name "Jon Kristensen")

(add-hook 'mu4e-compose-mode-hook
          (lambda ()
            (interactive)
            (save-excursion
              (end-of-buffer)
              (insert "\n\nMed vänliga hälsningar,\\\\\n\\\\\nJon Kristensen\\\\\nNejla AB\\\\\n031-330 31 00\n"))))

(setq message-citation-line-function 'message-insert-formatted-citation-line)
;; (%Y-%m-%d %R) can be used for date and time, but it appears to be UTC
(setq message-citation-line-format "%f skrev:")

;; (add-hook 'org-mime-html-hook (lambda () (org-mime-change-element-style "blockquote" "border-left: 2px red solid; margin: 0 0 1em 0; padding-left: 1ex;"))) ;; #ccc

;; (setq mml-secure-openphp-signers '("C5DF46E90F6EB0FF2013482C3D3B78D363B549EB"))
;; (add-hook 'message-send-hook 'mml-secure-message-sign-pgpmime)

(global-set-key [f3] 'mu4e)

(setq message-send-mail-function 'smtpmail-send-it)

(setq smtpmail-smtp-server "smtp.kolabnow.com"
              smtpmail-smtp-service 465
              smtpmail-stream-type 'ssl)

(add-hook 'org-mime-plain-text-hook
          (lambda ()
            (while (re-search-forward "\\\\\\\\$" nil t) (replace-match ""))))

;; https://www.djcbsoftware.nl/code/mu/mu4e/Bookmarks.html

(global-set-key [f3] 'mu4e)
;; (add-hook 'message-send-hook 'org-mime-htmlize)

;; Bookmarks for searches can be added with mu4e-bookmarks! (New syntax in 1.3.7)
;; b: Access bookmarks?

;; n: Next (p: Previous)
;; Enter: Open message
;; j: Jump to folder
;; zt: Turn off/on threading?
;; zr: Include related (om något är exkluderat från sökningen, till exempel trådar?)
;; d: Mark for deletion (D to delete immediately)
;; m: Mark as moved to folder
;; +: Mark as flagged
;; . (-?): Mark as unflagged
;; %: Mark accourding to pattern
;; u: Unmark message
;; x: Execute marks (Does not sync to the server immediately (M-x mu4e-update-mail-and-index, C-c C-u?))
;; s: Search messages (S: Modify the current search query) (Example: “to:whatever and from:whatever and (flag:read or flag:attach) and date:today..now”)
;; /: Narrow search?
;; g: Re-run the search in the view
;; C: Compose a new message
;; R: Reply
;; F: Forward
;; E: Edit draft

;; Sending messages:
;; Tab to auto-complete recipients; Separate recipients by comma)
;; C-c C-c: Send
;; C-c C-k: Cancel
;; C-c C-a: Add attachement
;; C-c C-d: Save to Drafts
;; (How to save it to the drafts?)

;; We can include Org Mode

;; Run mu4e in the background
;; (mu4e t)

;; (setq mail-user-agent 'mu4e-user-agent)

;; (setq org-msg-default-alternatives '((new . (text html))
;;                                      (reply-to-html . (text html))
;;                                      (reply-to-text . (text)))
;; 	    org-msg-convert-citation t
;;       org-msg-posting-style nil
;; 	    org-msg-signature "\n\nMed vänliga hälsningar,\n\n#+begin_signature\nJon Kristensen\\\\\nNejla AB\\\\\n031-330 31 00\n#+end_signature")

;; (org-msg-mode)

;; P: mu4e-headers-toggle-threading

(setq headers-show-threads nil)
