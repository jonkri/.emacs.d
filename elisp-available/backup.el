(setq auto-save-file-name-transforms `((".*" ,(expand-file-name "~/.emacs.d/backups"))))
(setq backup-directory-alist `(("." . ,(expand-file-name "~/.emacs.d/backups"))))
