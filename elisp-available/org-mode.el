(global-set-key (kbd "<f1>") (lambda () (interactive) (find-file "~/org/empty.org")))
(find-file "~/org/empty.org")

(global-set-key (kbd "<f2>") (lambda () (interactive) (org-agenda nil "A")))

(setq-default org-agenda-prefix-format '((todo . " %i %b") (tags . " %i %b")))
(setq-default org-agenda-files '("~/org/calendar.org" "~/org/todo.org"))
(setq-default org-todo-keywords '((sequence "BACKLOG(b!)" "READY(r!)" "IN PROGRESS(i!)" "WAITING(w@/!)" "|" "DONE(d!)" "CANCELLED(c@)")))
(setq-default org-stuck-projects '("LEVEL=2+project-incubated" ("READY" "IN PROGRESS" "WAITING")))
(setq-default org-capture-templates
              '(("p" "Project (Multiple actions)")
                ("pp"
                 "Project without action"
                 entry
                 (file+headline "~/org/todo.org" "Projects")
                 "** %^{Project headline} %^g\n%?%i")
                ("pb"
                 "Project with BACKLOG action"
                 entry
                 (file+headline "~/org/todo.org" "Projects")
                 "** %^{Project headline} %^g\n%?\n%i*** BACKLOG %^{Action headline}")
                ("pr"
                 "Project with READY action"
                 entry
                 (file+headline "~/org/todo.org" "Projects")
                 "** %^{Project headline} %^g\n%?\n%i*** READY [#%^{Priority|B|A|C}] %^{Action headline}")
                ("pi"
                 "Project with IN PROGRESS action"
                 entry
                 (file+headline "~/org/todo.org" "Projects")
                 "** %^{Project headline} %^g\n%?\n%i*** IN PROGRESS [#%^{Priority|B|A|C}] %^{Action headline}")
                ("pw"
                 "Project with WAITING action"
                 entry
                 (file+headline "~/org/todo.org" "Projects")
                 "** %^{Project headline} %^g\n%?\n%i*** WAITING [#%^{Priority|B|A|C}] %^{Action headline}")
                ("t" "Todo entry (Single action)")
                ("tn" "Non-project todo entry")
                ("tnb"
                 "BACKLOG project todo entry"
                 entry
                 (file "~/org/todo.org")
                 "* BACKLOG %^{Description} %^g\n%?%i\n")
                ("tnr"
                 "READY project todo entry"
                 entry
                 (file "~/org/todo.org")
                 "* READY [#%^{Priority|B|A|C}] %^{Description} %^g\n%?%i\n")
                ("tni"
                 "IN PROGRESS project todo entry"
                 entry
                 (file "~/org/todo.org")
                 "* IN PROGRESS [#%^{Priority|B|A|C}] %^{Description} %^g\n%?%i\n")
                ("tnw"
                 "WAITING project todo entry"
                 entry
                 (file "~/org/todo.org")
                 "* WAITING [#%^{Priority|B|A|C}] %^{Description} %^g\n%?%i\n")
                ("tp" "Project todo entry")
                ("tpb"
                 "BACKLOG non-project todo entry"
                 entry
                 (file+function "~/org/todo.org" goto-project)
                 "* BACKLOG %^{Description} %^g\n%?%i\n")
                ("tpr"
                 "READY non-project todo entry"
                 entry
                 (file+function "~/org/todo.org" goto-project)
                 "* READY [#%^{Priority|B|A|C}] %^{Description} %^g\n%?%i\n")
                ("tpi"
                 "IN PROGRESS non-project todo entry"
                 entry
                 (file+function "~/org/todo.org" goto-project)
                 "* IN PROGRESS [#%^{Priority|B|A|C}] %^{Description} %^g\n%?%i\n")
                ("tpw"
                 "WAITING non-project todo entry"
                 entry
                 (file+function "~/org/todo.org" goto-project)
                 "* WAITING [#%^{Priority|B|A|C}] %^{Description} %^g\n%?%i\n")
                ("s" "Specification entry" entry (file+function "~/org/specifications.org" goto-specification) "** %^{Description}\n:PROPERTIES:\n:DATE: %^{Date|%t}\n:HOURS: %^{Hours}\n:TIME: %^{Time}\n:DETAILS: %^{Details}\n:END:\n%i\n")
                ("c" "Calendar entry" entry (file "~/org/calendar.org") "* %^{Description} %^t %^g\n%i\n")))

(defun goto-project ()
  (interactive)
  (progn (goto-char (let* ((structure (org-element-parse-buffer 'headline))
                           (completions (org-element-map structure 'headline
                                          (lambda (headline) (when (and (= (org-element-property :level headline) 2)
                                                                        (member "PROJECT" (org-element-property :tags (org-element-property :parent headline))))
                                                               (list (org-element-property :title headline) headline))))))
                      (org-element-property :begin (cadr (assoc (completing-read "Project: " completions) completions)))))))

(defun goto-specification ()
  (interactive)
  (progn (goto-char (let* ((structure (org-element-parse-buffer 'headline))
                           (completions (org-element-map structure 'headline
                                          (lambda (headline) (when (= (org-element-property :level headline) 1)
                                            (list (org-element-property :title headline) headline))))))
                    (org-element-property :begin (cadr (assoc (completing-read "Project: " completions) completions)))))))

(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)

(setq-default org-agenda-custom-commands '(("A" "Custom Agenda" ((agenda "" ((org-agenda-span 14)))
                                                                 (todo "IN PROGRESS|READY|WAITING"
                                                                            ((org-agenda-overriding-header "\nUnscheduled non-BACKLOG items")
                                                                             (org-agenda-todo-ignore-scheduled 'all)))
                                                                 (stuck "" ((org-agenda-overriding-header "\nStuck projects")))))
                                           ("b" todo "BACKLOG" ((org-agenda-sorting-strategy '(tag-up priority-down))))))



(setq-default org-startup-indented t)

(setq-default org-icalendar-store-UID t)

(add-hook 'after-init-hook (lambda () (org-agenda nil "A")))
(setq-default org-agenda-window-setup 'current-window)

(setq-default org-agenda-skip-function-global '(org-agenda-skip-entry-if 'todo 'done))

;; SCHEDULED means the date when work on the item should start
;; A meeting, for example, should not use SCHEDULED, but a regular timestamp

(setq-default org-deadline-warning-days 0)
;; (setq-default org-agenda-skip-deadline-prewarning-if-scheduled t)
(setq-default org-agenda-start-on-weekday 1)
(setq-default calendar-week-start-day 1)

(setq-default org-todo-repeat-to-state "READY")
