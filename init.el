;; Enable Lisp files by creating a symlink in the elisp-enabled directory:
;; ln -s ~/.emacs.d/elisp-available/<file> ~/.emacs.d/elisp-enabled/
(require 'cl-lib)
(cl-loop for file in (directory-files "~/.emacs.d/elisp-enabled"
                                      nil
                                      directory-files-no-dot-files-regexp)
         do (if (and (file-readable-p (concat "~/.emacs.d/elisp-enabled/" file))
                     (not (string-equal file ".gitkeep")))
                (load (concat "~/.emacs.d/elisp-enabled/" file))))

(defalias 'yes-or-no-p 'y-or-n-p)

(setq browse-url-browser-function 'browse-url-firefox)
(setq browse-url-firefox-program "firefox-developer-edition")
(setq inhibit-startup-screen t)
(setq mode-line-position-column-line-format '(" (%l,%C)"))
(setq column-number-indicator-zero-based nil)
(setq mouse-wheel-follow-mouse 't)
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq require-final-newline 'visit-save)
(setq ring-bell-function 'ignore)
(setq scroll-step 1)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)

(blink-cursor-mode 0)
;; (cua-mode 1)
(electric-pair-mode 1)
(global-hl-line-mode 1)
(menu-bar-mode 0)
(set-face-attribute 'default nil :height 140)
(set-face-attribute 'mode-line nil :height 140)
(set-fringe-mode 0)
(show-paren-mode 1)
(toggle-scroll-bar 0)
(tool-bar-mode 0)

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(add-hook 'prog-mode-hook 'column-number-mode)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(global-set-key [f4] 'toggle-truncate-lines)
(global-set-key (kbd "C-<f4>") 'visual-line-mode)

(global-set-key [f5] 'browse-url-of-file)

(global-set-key (kbd "<M-S-down>") 'windmove-down)
(global-set-key (kbd "<M-S-left>") 'windmove-left)
(global-set-key (kbd "<M-S-right>") 'windmove-right)
(global-set-key (kbd "<M-S-up>") 'windmove-up)

(global-set-key (kbd "<C-s-up>") 'shrink-window)
(global-set-key (kbd "<C-s-down>") 'enlarge-window)
(global-set-key (kbd "<C-s-left>") 'shrink-window-horizontally)
(global-set-key (kbd "<C-s-right>") 'enlarge-window-horizontally)

(global-set-key (kbd "C-z") 'undo)
(global-set-key (kbd "C-v") 'yank)

(setq-default fill-column 80)

(add-hook 'org-mode-hook #'display-fill-column-indicator-mode)
(add-hook 'prog-mode-hook #'display-fill-column-indicator-mode)
(add-hook 'text-mode-hook #'display-fill-column-indicator-mode)

(setq confirm-kill-emacs 'y-or-n-p)

;; (add-to-list 'default-frame-alist '(fullscreen . maximized))
